import React, { Component } from 'react';
import { View, Text,AsyncStorage,PermissionsAndroid } from 'react-native';
import Input from './common/Input'
import Button from './common/Button'
import CardSection from './common/CardSection'
import Card from './common/Card'
class GeTLoc extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };
  }

  componentDidMount() {

 
  }
  
  removePrev(){
    AsyncStorage.removeItem('lat')
    AsyncStorage.removeItem('lon')
    alert('Data Removed')
  }
  getLocation(){
    navigator.geolocation.getCurrentPosition(
        (position) => {
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            error: null,
          });
        },
        (error) => this.setState({ error: error.message }),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
      alert(this.state.latitude , this.state.longitude)
      AsyncStorage.setItem('lat',this.state.latitude)
      AsyncStorage.setItem('lon',this.state.longitude)
  }

  render() {
    return (
      <View style={{marginTop:50,}} >
    
      <CardSection>
<Input placeholder="Enter Something"/>
</CardSection>
<CardSection>
<Button onPress={()=>this.getLocation()}>Get Location</Button>
<Button onPress={()=>this.removePrev()}>Remove</Button>
</CardSection>
     
        {this.state.error ? <Text> This is previous data: {this.state.error}</Text> : null}
      </View>
    );
  }
}

export default GeTLoc;