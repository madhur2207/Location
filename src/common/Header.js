// Import libraries for making a component
import React from 'react';
import { Text, View } from 'react-native';

// Make a component
const Header = (props) => {
  const { textStyle, viewStyle } = styles;

  return (
    <View style={viewStyle}>
      <Text style={textStyle}>{props.headerText}</Text>
    </View>
  );
};

const styles = {
  viewStyle: {
    backgroundColor: '#fff',
    height: 50,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative',justifyContent:'space-between'
  },
  textStyle: {
    color:'#000',
    fontWeight:'bold',
    fontSize: 20,
  marginLeft: 5,marginTop: 10
  }
};

// Make the component available to other parts of the app
export default Header ;
