import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#ff0000',
    fontSize: 16,
    fontWeight: '600',
   paddingTop: 5,
   paddingBottom: 5,
    alignItems: 'center',
  justifyContent: 'center'
  },
  buttonStyle: {
    flex: 1,
     backgroundColor: '#fff',
  borderColor: '#58ACFA',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderColor: '#ff0000',
    borderWidth:1,
marginTop: 50,marginLeft: 20,marginRight: 20,elevation:5
  }
};

export default Button ;
