import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ScrollView
} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import Per from './Per';
import GetLoc from './GetLoc';
export default class Rout extends Component{
    render(){
        return(
            <Router>
                <Scene headerMode="none" >
                <Scene key="get" component={GetLoc}/>
                <Scene key="show" component={Per}/>
              </Scene>

            </Router>
        )
    }
    }