import {View,Text,AsyncStorage, TextInput} from 'react-native'
import React,{Component} from 'react'
import Button from './common/Button'
import CardSection from './common/CardSection'
import Card from './common/Card'
import { Actions } from 'react-native-router-flux';
export default class Per extends Component{
    constructor(){
        super()
        this.state={
            lat:0,
            lon:0
        }
    }
    getData=async()=>{
this.state.lat=await AsyncStorage.getItem('lat')
this.state.lon=await AsyncStorage.getItem('lon')
    }
    componentWillMount(){
        this.getData()
    }
    render(){
        return(
            <View><Text>Latitude: {this.state.lat}</Text>
            <Text>Longitude: {this.state.lon}</Text>
            <CardSection><Button onPress={Actions.get}>Back</Button></CardSection>
            </View>
        )
    }
}